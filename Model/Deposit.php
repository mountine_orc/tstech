<?php
namespace Model
{
    use Core\Db;
            
    class Deposit
    {
        function __construct()
        {
            $dataBase = new Db;
            $this->db = $dataBase->connect();
        }
        
        function calculateRestDatesInterest()
        {
            $stmt = $this->db->prepare("UPDATE deposit 
                SET 
                    money = money + money/100*interest , 
                    lastinterestdate=NOW() 
                WHERE  
                    MONTH(lastinterestdate) != MONTH(NOW()) AND
                    DAY(createdate) != 31 AND
                    DATE_FORMAT(createdate, '%m-%d') != '04-17'");
            if ($stmt->execute())
                return $stmt->rowCount();
            else
                throw new \Exception ("Can not calculate rest dates interest");
        }
        
        function calculateSpecialDatesInterest()
        {
            $stmt = $this->db->prepare("UPDATE deposit 
                SET 
                    money = money + money/100*interest , 
                    lastinterestdate=NOW() 
                WHERE  
                    MONTH(lastinterestdate) != MONTH(NOW()) AND
                    (
                        (DAY(createdate) = 31 AND DAY(NOW()) = DAY(LAST_DAY(NOW()))) OR
                        (DATE_FORMAT(createdate, '%m-%d') = '04-17' AND DAY(NOW()) = 17)
                    )");
            if ($stmt->execute())
                return $stmt->rowCount();
            else
                throw new \Exception ("Can not calculate special dates interest");
        }
        
        
        function calculateFee()
        {
            $stmt = $this->db->prepare("UPDATE deposit 
                SET 
                    money = money -  (
                        CASE 
                            WHEN money < 1000 THEN IF(money/100*5 < 50, 50, money/100*5)
                            WHEN money < 10000 THEN money/100*6
                            WHEN money > 10000 THEN IF(money/100*7 > 5000, 5000, money/100*7)
                        END
                    ) * IF(
                        DATE_FORMAT(createdate, '%Y-%m') = DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m'), 
                        (DAY(LAST_DAY(createdate))-DAY(createdate)+1)/DAY(LAST_DAY(createdate)),
                        1
                    ) ,
                    lastfeedate = NOW()
                WHERE  
                    MONTH(lastfeedate) != MONTH(NOW())");
            if ($stmt->execute())
                return $stmt->rowCount();
            else
                throw new \Exception ("Can not calculate fee");
        }
        
    }
}
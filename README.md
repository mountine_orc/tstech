# Тестове завдання
Запускається командою. 

php command.php cron:depositcalculation

Код шукати тут Controller/CronController::depositcalculationAction()

Вся логіка знаходиться в SQL-запросах. На початку роботи було два варіанта: винести логіку в РНР чи в тіло запроса в базу.
Якщо в РНР то мені спадало на думку лиш вибрати всі записи, пробігтися по них циклом, і ті що підходять прогнати через бізнес логіку, створити нові запроси на апдейт і закинути в базу. 
Але запроси в циклі то є зло тому я вирішив піти шляхом винесення всьої логіки в сам запрос.Це ускладнює читання і підтримку але на великих обємах має дати значну перевагу в швидкості.

Крон налаштувати на запуск щодня. Команда сама буде відслідковувати які дні підходять для апдейтів.

```
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `inn` int(10) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthdate` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inn` (`inn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `client` (`id`, `firstname`, `lastname`, `inn`, `gender`, `birthdate`) VALUES
(1,	'vasya',	'petrov',	123,	'male',	'2017-08-23');

DROP TABLE IF EXISTS `deposit`;
CREATE TABLE `deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `money` float(10,2) NOT NULL,
  `interest` float(4,2) unsigned NOT NULL,
  `createdate` date NOT NULL,
  `lastinterestdate` date NOT NULL,
  `lastfeedate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `deposit_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `deposit` (`id`, `client_id`, `money`, `interest`, `createdate`, `lastinterestdate`, `lastfeedate`) VALUES
(1,	1,	106.15,	1.00,	'2017-04-17',	'2017-08-23',	'2017-08-23'),
```
#!/usr/bin/php
<?php

/* 
 * This file using for console commands
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("init.php");

use Core\Router;

try {  
    $router = new Router;
    $commandData = $router->getRouteFromCommandLine($argv); //['item', 'action', 'param']
}  
catch(\Exception $e) {  
    echo $e->getMessage();  
    die();
}

$controllerName = 'Controller\\'.ucfirst($commandData["item"]).'Controller';
$methodName = $commandData["action"]."Action";
$controller = new $controllerName();
$controller->$methodName($commandData["param"]);


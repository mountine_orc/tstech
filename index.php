<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("init.php");

use Core\Router;

$router = new Router;
$urlData = $router->getRouteFromUrl(); //['item', 'action', 'param']

$controllerName = 'Controller\\'.ucfirst($urlData["item"]).'Controller';
$methodName = $urlData["action"]."Action";
$controller = new $controllerName();
$controller->$methodName($urlData["param"]);


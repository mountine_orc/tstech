<?php
namespace Controller;

use Core\AbstractController;

class IndexController extends AbstractController{
    function indexAction()
    {
        $this->view->render("index");
    }
}
<?php
namespace Controller;

use Core\AbstractController;
use Model\Deposit;

class CronController extends AbstractController
{
    function __construct()
    {
        $this->deposit = new Deposit();
        parent::__construct();
    }
    
    function indexAction()
    {
        echo "Yo have to call some action";
    }
    
    function depositcalculationAction()
    {
        try {  
            $res1 = $this->deposit->calculateSpecialDatesInterest();
            $res2 = $this->deposit->calculateRestDatesInterest();
            $res3 = $this->deposit->calculateFee();
        }  
        catch(\Exception $e) {  
            echo $e->getMessage()."\n";  
        }
        
        echo "Command finished\n";
        echo "Calculated $res1 special dates interests\n";
        echo "Calculated $res2 rest dates interests\n";
        echo "Calculated $res2 fee\n";

    }
}
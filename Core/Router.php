<?php
namespace Core
{
    class Router
    {
        function getRouteFromUrl()
        {
            $actions = array();
            $actions["item"]=  "Index";
            $actions["action"]="Index";
            $actions["param"] = FALSE;
            
            $url = explode("/", $_SERVER['REQUEST_URI']);
            
            if (isset($url[1]) and trim($url[1]) != "")
                $actions["item"]=$url[1];
            
            if (isset($url[2]) and trim($url[2]) != ""){
                $actions["action"]=$url[2];
                
                if (isset($url[3])) 
                    $actions["param"]=$url[3];

                if ((string)(int)$url[2] == $url[2]){
                    $actions["action"]="get";
                    $actions["param"]=$url[2];
                }
            }
                

            
            return $actions;
        }
        
        function getRouteFromCommandLine($argv)
        {
            if (!isset($argv[1])){
                throw new \Exception ("There is no command line arguments");
            }
            
            
            if (substr_count($argv[1], ':') != 1){
                throw new \Exception ("Command line argument must be in controller:action format");
            }
            
            $argArray = explode(':', $argv[1]);
            
            $actions = [];
            $actions["item"] =  $argArray[0];
            $actions["action"] = $argArray[1];
            $actions["param"] = FALSE;

            return $actions;
        }
    }
}
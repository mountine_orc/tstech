<?php 
namespace Core;

use Core\Config;

class Db
{
    private static $db = FALSE;
    
    function __construct() {
        $this->dbconfig = (new Config)->getConfigArray()["db"];
    }

    function connect()
    {
        try {  
            if (Db::$db) 
                return Db::$db;

            $db = new \PDO("mysql:host=".$this->dbconfig["host"].";dbname=".$this->dbconfig["dbname"], $this->dbconfig["user"], $this->dbconfig["password"]);
            Db::$db = $db;

            return $db;
        }  
        catch(\PDOException $e) {  
            echo $e->getMessage();  
            die();
        }
    }


}

